#!/usr/bin/python
# =============================================================================
#        File : http_client.py
# Description : Allows sending a verb with data via HTTP over TCP/IP
#      Author : Drew Gislsason
#        Date : 3/8/2017
# =============================================================================
import httplib
import base64
import sys

# use this client for sending to 

HOST = "127.0.0.1"
PATH = '/my/api'
PORT = 5000

def show_help():
  print "Enter VERB URI Content"
  print "Examples:"
  print "  GET /my/api"
  print "  GET /my/api=?blah=true"
  print "  POST /my/api Hello World"
  print "  exit - leave the program\n"

HOST = raw_input('Enter Server IP address (e.g. 172.1.2.3) ')
if HOST == '':
  HOST = '127.0.0.1'

conn = httplib.HTTPConnection(HOST, 5000)

show_help()

while True:

  s = raw_input('HTTP$ ')
  if s == '?':
    show_help()
  elif s == 'exit':
    sys.exit(0)
  elif s[0:3] == 'GET':
    verb = 'GET'
  elif s[0:3] == 'PUT':
    verb = 'PUT'
  elif s[0:4] == 'POST':
    verb = 'POST'
  elif s[0:5] == 'DELETE':
    verb = 'DELETE'
  else:
    print "Invalid VERB"
    continue

  s = s[len(verb) + 1:]
  if s.find(' ') > 0:
    uri = s[0:s.find(' ')]
  else:
    uri = s

  data = ''
  if s.find(' ') > 0:
    data = s[s.find(' ')+1:]

  if verb == 'POST':
    conn.request(verb, uri, data )
  else:
    conn.request(verb, uri)
  r1 = conn.getresponse()

  print "status " + str(r1.status) + ", reason " + str(r1.reason)
  data1 = r1.read()
  print "return data: " + str(data1)

conn.close()
